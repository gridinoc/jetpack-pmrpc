const pageMod   = require("page-mod");
const tabs      = require("tabs");
const data      = require("self").data;

var workers = new Array();

pageMod.PageMod({
    include: ["http://www.mozilla*"],

    contentScriptWhen: 'ready',
    contentScriptFile: [
        data.url('jquery-1.5.1.js'),
        data.url('pmrpc/pmrpc.js'),
        data.url('pagemod.js')
    ],

    onAttach: function onAttach(worker) {
        workers.push(worker);
        console.log(worker.url);
        worker.on('message', function (message) {
            console.log(message);
            workers.forEach(
                function (_worker) {
                    if (_worker && _worker != worker) _worker.postMessage(message);
                }
            );
        });
    }
});

tabs.open("http://www.mozilla.com");
tabs.open("http://www.mozilla.org");


console.log("The rpc-test add-on is running.");
