function log (message) {
    // if we have firebug
    if (window.console && window.console.log)
        window.console.log(message);
}

log(self);

// on click prompt for name and invoke hello(name)
$('body').click(function () {
    pmrpc.call({
        destination:            self,
        publicProcedureName:    "hello",
        params:                 [prompt("Name")],
        onSuccess: function (returnObj) {
            log(returnObj);
            alert(returnObj.returnValue);
        },
    });
});

// respond to hello(name)
pmrpc.register({
    publicProcedureName:    "hello",
    procedure:  function (printParam) {
        log('received ' + printParam);
        return "Hello " + printParam + " from " + document.title;
    },
    isAsynchronous: false
});


// self.on('message', function (message) {
//     log(message);
//     pmrpc.processPmrpcMessage({
//             event: {
//                 data: message,
//                 source: self,
//                 origin: self,
//                 shouldCheckACL : false
//             },
//             source: self 
//         });
// });
